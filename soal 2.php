<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
    <?php
    echo "<h3> Soal No 1 Greetings </h3>";
    
    // Fungsi greetings
    function greetings($name) {
        echo "Halo " . ucfirst($name) . ", Selamat Datang di Garuda Cyber Institute!";
    }
    
    greetings("Bagas");
     echo "<br>";
    greetings("Wahyu");
     echo "<br>";
    greetings("nama peserta");
    
    echo "<br>"; echo "<h3>Soal No 2 Reverse String</h3>";
    
    // Fungsi reverseString
    function reverseString($str) {
        $reverse = '';
        for ($i = strlen($str) - 1; $i >= 0; $i--) {
            $reverse .= $str[$i];
        }
        echo $reverse;
    }
    
    reverseString("nama peserta");
     echo "<br>";
    reverseString("Garuda Cyber Institute");
     echo "<br>";
    reverseString("We Are GC-Ins Developer");
    
    echo "<br>"; echo "<h3>Soal No 3 Palindrome </h3>";
    
    // Fungsi palindrome
    function palindrome($str) {
        $reversedStr = reverseString($str);
        if ($str === $reversedStr) {
            echo "true";
        } else {
            echo "false";
        }
    }
    
    palindrome("civic"); // true
    echo "<br>";
    palindrome("nababan"); // true
     echo "<br>";
    palindrome("jambaban"); // false
     echo "<br>";
    palindrome("racecar"); // true
    
    echo "<h3>Soal No 4 Tentukan Nilai </h3>";
    
    // Fungsi tentukan_nilai
    function tentukan_nilai($nilai) {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            return "Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            return "Cukup";
        } else {
            return "Kurang";
        }
    }
    
    echo tentukan_nilai(98); // Sangat Baik
    echo "<br>";
    echo tentukan_nilai(76); // Baik
    echo "<br>";
    echo tentukan_nilai(67); // Cukup
    echo "<br>";
    echo tentukan_nilai(43); // Kurang
    ?>
</body>
</html>
